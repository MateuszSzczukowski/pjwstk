package pl.mateusz_szczukowski;

/**
 * Created by Mateusz on 15.10.2016.
 */
public class Address {
    public String ulica;
    public int nrDomu;
    public int kodPocztowy;


    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getNrDomu() {
        return nrDomu;
    }

    public void setNrDomu(int nrDomu) {
        this.nrDomu = nrDomu;
    }

    public int getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(int kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }
}
