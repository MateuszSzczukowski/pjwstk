package pl.mateusz_szczukowski;

/**
 * Created by Mateusz on 15.10.2016.
 */
public class Person {
    public String imie;
    public String nazwisko;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
