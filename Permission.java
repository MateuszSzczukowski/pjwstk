package pl.mateusz_szczukowski;

/**
 * Created by Mateusz on 15.10.2016.
 */
public class Permission {
    public String permission;

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
