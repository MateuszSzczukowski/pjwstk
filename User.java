package pl.mateusz_szczukowski;

/**
 * Created by Mateusz on 15.10.2016.
 */
public class User {
    public String Login;
    public String haslo;

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
}
